#include "mainwindow.h"
#include "dialog.h"
#include "mainform.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    MainForm h;
    h.show();
    return a.exec();
}
